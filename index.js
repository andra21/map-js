const dateFormatYmd = d3.timeFormat('%Y-%m-%d');
const dateFormatYmdParse = d3.timeParse('%Y-%m-%d');
const dateFormatdMYParse = d3.timeParse('%d-%b-%Y');
const dateFormatmdYParse = d3.timeParse('%m/%d/%Y');
const dateFormatYW = d3.timeFormat('%Y-%W');
const dateFormatYWParse = d3.timeParse('%Y-%W');
const numberFormat = d3.format(',.0f');
const percentFormat = d3.format('.1%');
const percent3Format = d3.format('.3%');
const permilleFormat = function(n) {
  return (Number(n)*1000).toFixed(2) + "‰";
};
const bisectKey = d3.bisector((d) => { return d.key; });

const styles = {
  sit_1: {     // @see https://paletton.com/#uid=b2A030H0kkhJP26s087JitZNnVzJE
    mainColor: '#FF9710',
    avgColor: '#FFE3BE',
    avgDashStyle: [1, 2],
  },
  sit_2: {
    mainColor: '#77F30F',
    avgColor: '#CEF0B3',
    avgDashStyle: [1, 2],
  },
  sit_3: {
    mainColor: '#FF2210',
    avgColor: '#FFC3BE',
    avgDashStyle: [1, 2],
  },
};

var dimensions = {
  panel1_width: 1040,
  panel1_height: 800,
};

const translations = {
  date: 'valoare',
  sit_1: 'sit1',
  sit_3: 'sit2',
  sit_2: 'sit3',
};


function loadCSV_Judete() {
  return d3.dsv(",", "data/Situaţia tinerilor NEETs înregistraţi la SPO în cadrul proiectului Intespo în luna MAI 2018.csv")
  
    .then(function (allRows) {
     return remapCSVWithRules(allRows, [
        {
          key: 'key',
          primary: true,
          convert: function (v) { return v; },
          regexp: new RegExp('^JUDET$')
        },
        {
          key: 'value',
          primary: false,
          convert: function (v) { return +v; },
          regexp: new RegExp('^VALOARE$')
        }
      ]);

    });
}


function remapCSVWithRules(allRows, remapRules) {
  var remappedKeys = {};
  var firstRow = Object.entries(allRows[0]);
  firstRow.forEach((d) => {
    for (var ri = 0; ri < remapRules.length; ri++) {
      if (d[0].match(remapRules[ri].regexp)) {
        remappedKeys[d[0]] = {
          key: remapRules[ri].key,
          rule: remapRules[ri]
        };
        break;
      }
    }
  });
  var remappedRows = [];


  let primaryKeys = [];
  let values = [];
  for (k in remappedKeys) {
    if (remappedKeys[k].rule.primary) {
      primaryKeys.push({
        key: k,
        rule: remappedKeys[k].rule
      });
    }
    else {
      values.push({
        key: k,
        rule: remappedKeys[k].rule
      });
    }
  }


  allRows.forEach((d) => {
    for (var iv in values) {
      let nd = {
        key: null,
        value: null
      };
      for (var ip in primaryKeys) {
        nd.key = primaryKeys[ip].rule.convert(d[primaryKeys[ip].key]);
      }

      
      nd.value = values[iv].rule.convert(d[values[iv].key]);

      remappedRows.push(nd);
    }
  });

  return remappedRows;
}

function generateChart_mapChart(mapChart, dimension_cities, citiesValues, citiesJson) {
  const extentForAllCities = d3.extent(citiesValues.all(), (d) => {
    return d.value;
  });
  extentForAllCities[1]+=1/100000000; // make sure min != max, always

  mapChart.width(dimensions.panel1_width)
    .height(dimensions.panel1_height)
    .transitionDuration(200)
    .dimension(dimension_cities)
    .group(citiesValues)
    .projection(d3.geoMercator()
      .fitSize([dimensions.panel1_width, dimensions.panel1_height], citiesJson)
    )
    .overlayGeoJson(citiesJson.features, "city", (d) => {
      let city = d.properties.mnemonic.toUpperCase();
      return (city == 'B' ? 'BUC' : city);
    })
    .colors(  // class color red for small values
      d3.scalePow().exponent(0.3)
        // d3.scaleLog().base(2)  // <-- this gives black colors for some cases
        .domain([
          Math.min(0, extentForAllCities[0]),
          extentForAllCities[1] * 0.10,
          extentForAllCities[1] * 0.25,
          extentForAllCities[1] * 1.00,
        ])
        .range(["#F9FFF3", "#CBFC99", "#FF8058", "#FF3D00"]) // @see http://paletton.com/#uid=51n1b0kl1Wx1x+IcEXDsUWkWEVB
    )         
    .valueAccessor((d) => {
      return d.value;
    })
    .on('pretransition', (chart) => {
      // re-scale color on the filtered data
      const extentForAllCities = d3.extent(chart.group().all(), (d) => {
        return d.value;
      });
      chart.colors()
        .domain([
          Math.min(0, extentForAllCities[0]),
          extentForAllCities[1] * 0.10,
          extentForAllCities[1] * 0.25,
          extentForAllCities[1] * 1.00,
        ]);


        // put labels in te middle
        window.chart = chart;
        var projection = chart.projection();
        var labelG = chart.select("svg")
            .selectAll('g.labels').data([0])
            .enter()
            .append("svg:g")
            .attr("class", "labels");

        labelG.selectAll("text")
            // .data(chart.group().all())
            .data(citiesJson.features)
            .enter().append("svg:text")
            .html(function(d) {
              if (d.properties.mnemonic=='B') {
                return 1;
              }
              if (d.properties.mnemonic=='IF') {
                return 2;
              }
              let allRows = chart.group().all();
              let row = allRows.filter( (r) => { return r.key==d.properties.mnemonic; } );
              return `<tspan dx="0" dy="1em">${d.properties.mnemonic}</tspan><tspan dx="-1em" dy="1em">${row[0]?row[0].value:0}</tspan>`;
            })
            .attr("x", (d) => { 
              let coord = d.geometry.coordinates[0];
              let point = d3.geoPath().centroid(d.geometry);
              return projection(point)[0];
            })
            .attr("y", (d) => { 
              let coord = d.geometry.coordinates[0];
              let point = d3.geoPath().centroid(d.geometry);
              return projection(point)[1];
            })
            // .attr("dx", "-1em")
            .classed("city-label", true);
    });

    return mapChart;
}

function generateChart_dataTableJquery(dataTable, totalsDimension) {
  dataTable
    .dimension(totalsDimension)
    .size(10)
    .sortBy((d) => {
      return d.key;
    })
    .order(d3.descending);
    return dataTable;
}
///////


////////// trebuie sa preia bifa din interfata si sa interpreteze daca este bifata small red sa coloreze harta cu .colors else colors1


  function renderOutput(){
   var fr = new FormData($('#form')[0]); //declar variabila fr apoi creez un obiect formData apeland un selector de tip id din formular incepand cu 1 
    var title = fr.get("title");
    $("#map-title").html(title);
    console.log(title);

   
    if (fr.get('colorTheme')=='theme1') { //green for big values
      mapChart1.colors().range(['#f00', '#0f0']);
    }
    if (fr.get('colorTheme')=='theme2') { //red for big values
      mapChart1.colors().range(['#0f0', '#f00']);
    }
    dc.redrawAll();
    

    


/*
    .colors1(  // new class color red for big values
      d3.scalePow().exponent(0.3)
        // d3.scaleLog().base(2)  // <-- this gives black colors for some cases
        .domain([
          Math.min(0, extentForAllCities[0]),
          extentForAllCities[1] * 0.10,
          extentForAllCities[1] * 0.25,
          extentForAllCities[1] * 1.00,
        ])
        .range(["#FF3D00", "#FF8058", "#CBFC99", "#F9FFF3"]) // @see http://paletton.com/#uid=51n1b0kl1Wx1x+IcEXDsUWkWEVB
    )
    */

   // ($('#form')[0]); // declar variabila formData ci mentionez id formularului
  //  FormData = formData.get('form[title]'); // 
  //   console.log(form);
  }
  