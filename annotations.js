class Annotations {
  constructor(chart) {
    this.chart = chart;
    // this.linearInterpolator = d3.interpolate('linear');
    this.lineBuilder = d3.line().curve(d3.curveLinear);
    this.areaBuilder = d3.area().curve(d3.curveLinear);
    this.dayWidth = 0.001;
  }

  setupDayWidth(day1, day2) {
    this.dayWidth =  Math.abs(this.chart.x()(day1) - this.chart.x()(day2));
    return this;
  }

  annotate(points) {
    var selection = this.chart.g()
      .select('g.annotation-group')
      .remove();

    let annotations = [];
    for (let i=0; i<points.length; i++) {
      let d = points[i];

      var annotation = {
        note: {
          title: d.label,
          // label: "Longer text to show text wrapping",
        },
        x:  this.chart.margins().left + this.chart.x()(d.keys[0]) - this.dayWidth/2,
        y:  this.chart.margins().top + this.chart.y().range()[0],
        // data: { date: "18-Sep-09", close: 185.02 },
        dy: -1 * this.chart.y().range()[0] * 0.9,
        // dx: -0.1,
        // type: d3.annotationCalloutRect,
        // subject: {
        //   width: this.chart.x()(d.keys[1]) - this.chart.x()(d.keys[0]) - this.dayWidth,
        //   height: ((this.chart.y().range()[1] - this.chart.y().range()[0])) * 0.90
        // }
      };

      // @see http://paletton.com/#uid=7000G0kuIt-d9K3m9wKyFldMec7
      // @see http://paletton.com/#uid=7000G0kbRt14+E48dwffUpTkImm
      if (d.type=="event:bad") {
        annotation.color = ["#A90000",];
        annotation.type = d3.annotationCalloutCircle;
        annotation.x+= this.dayWidth/2;
        annotation.subject = {
          radius: this.dayWidth/2, 
          radiusPadding: this.dayWidth/6
        };
      }
      if (d.type=="event:good") {
        annotation.color = ["#3FCD3F",];
        annotation.type = d3.annotationCalloutCircle;
        annotation.x+= this.dayWidth/2;
        annotation.subject = {
          radius: this.dayWidth/2, 
          radiusPadding: this.dayWidth/6
        };
      }
      if (d.type=="holiday") {
        annotation.color = ["#608094",];
        annotation.type = d3.annotationCalloutRect;
        annotation.dy*= 0.75;
        annotation.subject = {
          // width: this.chart.x()(d.keys[0]) - this.chart.x()(d.keys[1]) - this.dayWidth,
          width: Math.abs(this.chart.x()(d.keys[0]) - this.chart.x()(d.keys[1])) + this.dayWidth,
          height: ((this.chart.y().range()[1] - this.chart.y().range()[0])) * 0.90
        };
      }

      annotations.push(annotation);
    }
    const makeAnnotations = d3.annotation()
      .type(d3.annotationLabel)
      .annotations(annotations);

    this.chart.g()
      .append("g")
      .lower()
      .attr("class", "annotation-group")
      .call(makeAnnotations)

  }
}
